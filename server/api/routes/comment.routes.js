import { Router } from 'express';
import * as commentService from '../services/comment.service';

const router = Router();

router
    .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
        .then(comment => res.send(comment))
        .catch(next))
    .post('/', (req, res, next) => commentService.create(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then(comment => res.send(comment))
        .catch(next))
    .delete('/:id', (req, res, next) => commentService.deleteCommentById(req.params.id)
        .then(response => res.send(response))
        .catch(next))
    .put('/', (req, res, next) => commentService.updateComment(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then((response) => res.send(response))
        .catch(next))
    .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then((reaction) => {

            return res.send(reaction);
        })
        .catch(next))
    .get('/restore/:id', (req, res, next) => commentService.restoreComment(req.params.id)
        .then((response) => res.send(response))
        .catch(next))
export default router;
