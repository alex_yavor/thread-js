import commentRepository from '../../data/repositories/comment.repository';
import commentReactionRepository from '../../data/repositories/comment-reaction.repository';
export const create = (userId, comment) => commentRepository.create({
    ...comment,
    userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const deleteCommentById = async id => {

    const result = await commentRepository.deleteById(id);
    return Number.isInteger(result) ? { deleted: true } : { deleted: false }
};

export const updateComment = async (userId, comment) => {

    const result = await commentRepository.updateComment(userId, comment);
    const updated = Number(result) === 1 ? true : false;
    return { updated }
};

export const restoreComment = async (commentId) => {
    const result = await commentRepository.restoreComment(commentId);
    const restored = result ? true : false;
    return { restored }
};

export const setReaction = async (userId, { commentId, isLike = true }) => {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
        ? commentReactionRepository.deleteById(react.id)
        : commentReactionRepository.updateById(react.id, { isLike }));

    const reaction = await commentReactionRepository.getPostReaction(userId, commentId);

    const result = reaction
        ? await updateOrDelete(reaction)
        : await commentReactionRepository.create({ userId, commentId, isLike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result) ? {} : commentReactionRepository.getPostReaction(userId, commentId);
};