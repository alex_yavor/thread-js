import postRepository from '../../data/repositories/post.repository';
import postReactionRepository from '../../data/repositories/post-reaction.repository';
import userRepository from '../../data/repositories/user.repository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
    ...post,
    userId
});

export const setReaction = async (userId, { postId, isLike = true }) => {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
        ? postReactionRepository.deleteById(react.id)
        : postReactionRepository.updateById(react.id, { isLike }));

    const reaction = await postReactionRepository.getPostReaction(userId, postId);

    const result = reaction
        ? await updateOrDelete(reaction)
        : await postReactionRepository.create({ userId, postId, isLike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};

export const deletePostById = async id => {

    const result = await postRepository.deleteById(id);
    return Number.isInteger(result) ? { deleted: true } : { deleted: false }
};

export const updatePost = async (userId, post) => {

    const result = await postRepository.updatePost(userId, post);
    // console.log(result);
    const updated = Number(result) === 1 ? true : false;
    return { updated }
};

export const restorePost = async (postId) => {
    const result = await postRepository.restorePost(postId);
    const restored = result ? true : false;
    return { restored }
};

export const getReactedUsers = async (data) => {
    console.log('getReactedUsers',data)
    const users = await userRepository.getReactedUsers(data);
    return users
};

