import { UserModel, ImageModel, PostReactionModel, CommentReactionModel } from '../models/index';
import BaseRepository from './base.repository';

class UserRepository extends BaseRepository {
    addUser(user) {
        return this.create(user);
    }

    getByEmail(email) {
        return this.model.findOne({ where: { email } });
    }

    getByUsername(username) {
        return this.model.findOne({ where: { username } });
    }

    getUserById(id) {
        return this.model.findOne({
            group: [
                'user.id',
                'image.id'
            ],
            where: { id },
            include: {
                model: ImageModel,
                attributes: ['id', 'link']
            }
        });
    }

    async getReactedUsers(data) {
        console.log(data)
        const id = data.postId ? data.postId : data.commentId;
        const nameId = data.postId ? 'postId' : 'commentId';
        const { isLike } = data;
        return this.model.findAll({
            include: [{
                model: ImageModel,
                attributes: ['id', 'link']
            },
            {
                model: data.postId ? PostReactionModel : CommentReactionModel,
                where: {
                    [nameId]: id,
                    isLike
                }
            }],
        })
    }
}

export default new UserRepository(UserModel);
