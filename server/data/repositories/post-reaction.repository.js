import { PostReactionModel, PostModel } from '../models/index';
import BaseRepository from './base.repository';
import sequelize from '../db/connection';
const likeCase = bool => `CASE WHEN "isLike" = ${bool} THEN 1 ELSE 0 END`;
class PostReactionRepository extends BaseRepository {
    getPostReaction(userId, postId) {
        return this.model.findOne({
            group: [
                'postReaction.id',
                'post.id'
            ],
            where: { userId, postId },
            // attributes: {
            //     include: [
            //         [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
            //         [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
            //     ]
            // },
            include: [{
                model: PostModel,
                attributes: ['id', 'userId']
            }]
        });
    }
}

export default new PostReactionRepository(PostReactionModel);
