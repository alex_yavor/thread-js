import { CommentReactionModel, CommentModel } from '../models/index';
import BaseRepository from './base.repository';
import sequelize from '../db/connection';
const likeCase = bool => `CASE WHEN "isLike" = ${bool} THEN 1 ELSE 0 END`;
class CommentReactionRepository extends BaseRepository {
    getPostReaction(userId, commentId) {
        return this.model.findOne({
            group: [
                'commentReaction.id',
                'comment.id'
            ],
            where: { userId, commentId },
            // attributes: {
            //     include: [
            //         [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
            //         [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
            //     ]
            // },
            include: [{
                model: CommentModel,
                attributes: ['id', 'userId']
            }]
        });
    }
}

export default new CommentReactionRepository(CommentReactionModel);
