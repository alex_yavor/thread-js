import sequelize from '../db/connection';
import { PostModel, CommentModel, UserModel, ImageModel, PostReactionModel, CommentReactionModel } from '../models/index';
import BaseRepository from './base.repository';
import { Op } from 'sequelize'
const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;
const commentLikeCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;
class PostRepository extends BaseRepository {
    async getPosts(filter) {
        const {
            from: offset,
            count: limit,
            userId,
            showAnotherPosts,
            showLikedPosts
        } = filter;
        const where = {};
        if (showLikedPosts) {
            // cons
            Object.assign(where, {
                // userId:{[Op.col]: 'postReactions.userId'},
                // true:{[Op.col]: 'postReactions.isLike'}
            });
        }
        else if (showAnotherPosts) {
            Object.assign(where, { userId: { [Op.ne]: userId } });
        }
        else if (userId) {
            Object.assign(where, { userId });
        }
        // console.log(where)

        return this.model.findAll({
            where,
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        and "comment"."deletedAt" IS NULL)`), 'commentCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount'],
                ]
            },
            include: [{
                model: ImageModel,
                attributes: ['id', 'link']
            }, {
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }, {
                model: PostReactionModel,
                attributes: [],
                duplicating: false,
                // where: showLikedPosts ? {
                //     userId,
                //     isLike: true
                // } : {}
            }],
            group: [
                'post.id',
                'image.id',
                'user.id',
                'user->image.id'
            ],
            order: [['createdAt', 'DESC']],
            offset,
            limit
        });
    }

    getPostById(id) {
        return this.model.findOne({
            group: [
                'post.id',
                'comments.id',
                'comments->commentReactions.id',
                'comments->user.id',
                'comments->user->image.id',
                'user.id',
                'user->image.id',
                'image.id',
            ],
            where: { id },
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" 
                        and "comment"."deletedAt" IS NULL )`), 'commentCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
                ]
            },
            include: [{
                model: CommentModel,
                // attributes: {
                //     include: [
                //         [sequelize.fn('SUM', sequelize.col('"postReactions"."isLike"')), 'likeCount'],
                //         // [sequelize.fn('SUM', sequelize.literal(commentLikeCase(false))), 'dislikeCount']
                //     ]
                // },
                include: [{
                    model: UserModel,
                    attributes: ['id', 'username','status'],
                    include: {
                        model: ImageModel,
                        attributes: ['id', 'link']
                    }
                }, {
                    model: CommentReactionModel,
                    attributes: ['isLike', 'id']
                }]
            }, {
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }, {
                model: ImageModel,
                attributes: ['id', 'link']
            }, {
                model: PostReactionModel,
                attributes: []
            }]
        });
    }


    updatePost(userId, post) {
        const { body, id, imageId } = post;
        console.log('updatepost repo', post, userId)
        return this.model.update({
            body,
            imageId,
        }, {
                where: {
                    id,
                    userId
                }
            });
    }

    restorePost(id) {
        return this.model.restore({
            where: {
                id
            }
        });
    }



}

export default new PostRepository(PostModel);
