import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { updateProfile } from './actions';
import {
    Grid,
    Image,
    Input,
    Button
} from 'semantic-ui-react';



const Profile = ({ user, updateProfile }) => {
    const [username, setUsername] = useState(user.username);
    const [email, setEmail] = useState(user.email);
    const [status, setStatus] = useState(user.status);

    function handleClick() {
        console.log('clicked');
        updateProfile({ ...user, username, email, status });
    }

    return (<Grid container textAlign="center" style={{ paddingTop: 30 }}>
        <Grid.Column>
            <Image centered src={getUserImgLink(user.image)} size="medium" circular />
            <br />
            <Input
                icon="user"
                iconPosition="left"
                placeholder="Username"
                type="text"
                value={username}
                onChange={ev => setUsername(ev.target.value)}
            />
            <br />
            <br />
            <Input
                icon="at"
                iconPosition="left"
                placeholder="Email"
                type="email"
                value={email}
                onChange={ev => setEmail(ev.target.value)}
            />
            <br />
            <br />
            <Input
                icon="info"
                iconPosition="left"
                placeholder="status"
                type="text"
                value={status || ""}
                onChange={ev => setStatus(ev.target.value)}
            />
            <br />
            <br />
            <Button
                content='Save Changes'
                onClick={handleClick}
            />
        </Grid.Column>
    </Grid>
    )
};

Profile.propTypes = {
    user: PropTypes.objectOf(PropTypes.any)
};

Profile.defaultProps = {
    user: {}
};

const mapStateToProps = rootState => ({
    user: rootState.profile.user
});

const actions = {
    updateProfile
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);
