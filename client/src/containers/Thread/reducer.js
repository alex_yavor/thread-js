import {
    SET_ALL_POSTS,
    LOAD_MORE_POSTS,
    ADD_POST,
    SET_EXPANDED_POST,
    SET_EDITABLE_POST,
    SET_EDITABLE_COMMENT,
    SET_POST_REACTED_USERS,
    SET_COMMENT_REACTED_USERS
} from './actionTypes';

export default (state = {}, action) => {
    switch (action.type) {
        case SET_ALL_POSTS:
            return {
                ...state,
                posts: action.posts,
                hasMorePosts: Boolean(action.posts.length)
            };
        case LOAD_MORE_POSTS:
            return {
                ...state,
                posts: [...(state.posts || []), ...action.posts],
                hasMorePosts: Boolean(action.posts.length)
            };
        case ADD_POST:
            return {
                ...state,
                posts: [action.post, ...state.posts]
            };
        case SET_EXPANDED_POST:
            return {
                ...state,
                expandedPost: action.post
            };
        case SET_EDITABLE_POST:
            return {
                ...state,
                editablePost: action.post
            };
        case SET_EDITABLE_COMMENT:
            return {
                ...state,
                editableComment: action.comment
            };
        case SET_COMMENT_REACTED_USERS:
            return {
                ...state,
                commentReactedUsers: action.users
            };
        case SET_POST_REACTED_USERS:
            return {
                ...state,
                postReactedUsers: action.users
            };
        default:
            return state;
    }
};
