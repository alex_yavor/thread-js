import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
    loadPosts,
    loadMorePosts,
    likePost,
    toggleExpandedPost,
    addPost,
    deletePost,
    getUpdatePost,
    updatePost,
    restorePost,
    getReactedUsers
} from './actions';
import { Modal } from 'semantic-ui-react';
import styles from './styles.module.scss';

class Thread extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sharedPostId: undefined,
            editPostId: undefined,
            showOwnPosts: false,
            showAnotherPosts: false,
            showLikedPosts: false
        };
        this.postsFilter = {
            userId: undefined,
            from: 0,
            count: 10
        };
    }

    tooglePosts = () => {
        this.setState(
            ({ showOwnPosts, showAnotherPosts, showLikedPosts }) => ({
                showOwnPosts: !showOwnPosts,
                showAnotherPosts: showAnotherPosts ? !showAnotherPosts : showAnotherPosts,
                showLikedPosts: showLikedPosts ? !showLikedPosts : showLikedPosts
            }),
            () => {
                Object.assign(this.postsFilter, {
                    userId: this.state.showOwnPosts ? this.props.userId : undefined,
                    from: 0,
                    showAnotherPosts: undefined,
                    showLikedPosts: undefined
                });
                this.props.loadPosts(this.postsFilter);
                this.postsFilter.from = this.postsFilter.count; // for next scroll
            }
        );
    };

    anotherPosts = () => {
        this.setState(
            ({ showAnotherPosts, showOwnPosts, showLikedPosts }) => ({
                showAnotherPosts: !showAnotherPosts,
                showOwnPosts: showOwnPosts ? !showOwnPosts : showOwnPosts,
                showLikedPosts: showLikedPosts ? !showLikedPosts : showLikedPosts
            }),
            () => {
                Object.assign(this.postsFilter, {
                    userId: this.state.showAnotherPosts ? this.props.userId : undefined,
                    from: 0,
                    showAnotherPosts: this.state.showAnotherPosts ? true : undefined,
                    showLikedPosts: undefined
                });
                this.props.loadPosts(this.postsFilter);
                this.postsFilter.from = this.postsFilter.count; // for next scroll
            }
        );
    };

    likedPosts = () => {
        this.setState(
            ({ showAnotherPosts, showOwnPosts, showLikedPosts }) => ({
                showAnotherPosts: showAnotherPosts ? !showAnotherPosts : showAnotherPosts,
                showOwnPosts: showOwnPosts ? !showOwnPosts : showOwnPosts,
                showLikedPosts: !showLikedPosts
            }),
            () => {
                Object.assign(this.postsFilter, {
                    userId: this.state.showLikedPosts ? this.props.userId : undefined,
                    from: 0,
                    showLikedPosts: this.state.showLikedPosts ? true : undefined
                });
                this.props.loadPosts(this.postsFilter);
                this.postsFilter.from = this.postsFilter.count; // for next scroll
            }
        );
    };


    loadMorePosts = () => {
        this.props.loadMorePosts(this.postsFilter);
        const { from, count } = this.postsFilter;
        this.postsFilter.from = from + count;
    }

    sharePost = (sharedPostId) => {
        this.setState({ sharedPostId });
    };

    editPost = (editPostId) => {
        console.log('postReactedUsers', this.props.postReactedUsers);
        this.props.getUpdatePost(editPostId);
        this.setState({ editPostId });
    };

    closeSharePost = () => {
        this.setState({ sharedPostId: undefined });
    }
    closeEditPost = () => {
        console.log('close')
        this.setState({ editPostId: undefined });
    }

    uploadImage = file => imageService.uploadImage(file);

    render() {
        const { posts = [], expandedPost, hasMorePosts, ...props } = this.props;
        const { showOwnPosts, sharedPostId, showAnotherPosts, showLikedPosts, editPostId } = this.state;
        return (
            <div className={styles.threadContent}>
                <div className={styles.addPostForm}>
                    <AddPost addPost={props.addPost} uploadImage={this.uploadImage} />
                </div>
                <div className={styles.toolbar}>
                    <Checkbox toggle label="Show only my posts" checked={showOwnPosts} onChange={this.tooglePosts} />
                </div>
                <div className={styles.toolbar}>
                    <Checkbox toggle label="Show not my posts" checked={showAnotherPosts} onChange={this.anotherPosts} />
                </div>
                <div className={styles.toolbar}>
                    <Checkbox toggle label="Show liked posts" checked={showLikedPosts} onChange={this.likedPosts} />
                </div>
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadMorePosts}
                    hasMore={hasMorePosts}
                    loader={<Loader active inline="centered" key={0} />}
                >
                    {posts.map(post => (
                        <Post
                            post={post}
                            likePost={props.likePost}
                            toggleExpandedPost={props.toggleExpandedPost}
                            sharePost={this.sharePost}
                            key={post.id}
                            canEdit={post.userId === this.props.userId ? true : false}
                            editPost={this.editPost}
                            deletePost={props.deletePost}
                            restorePost={props.restorePost}
                            getReactedUsers={props.getReactedUsers}
                            postReactedUsers={props.postReactedUsers}
                        />
                    ))}
                </InfiniteScroll>
                {
                    expandedPost
                    && <ExpandedPost sharePost={this.sharePost} />
                }
                {
                    sharedPostId
                    && <SharedPostLink postId={sharedPostId} close={this.closeSharePost} />
                }
                {
                    editPostId
                    && <Modal open onClose={this.closeEditPost}>
                        <Modal.Content>
                            <AddPost
                                addPost={props.updatePost}
                                uploadImage={this.uploadImage}
                                post={props.editablePost}
                                close={this.closeEditPost} />
                        </Modal.Content> </Modal>
                }
            </div>
        );
    }
}

Thread.propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object),
    hasMorePosts: PropTypes.bool,
    expandedPost: PropTypes.objectOf(PropTypes.any),
    sharedPostId: PropTypes.string,
    userId: PropTypes.string,
    loadPosts: PropTypes.func.isRequired,
    loadMorePosts: PropTypes.func.isRequired,
    likePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    addPost: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired,
    getUpdatePost: PropTypes.func.isRequired,
    updatePost: PropTypes.func.isRequired,
    restorePost: PropTypes.func.isRequired,
    getReactedUsers: PropTypes.func.isRequired
};


Thread.defaultProps = {
    posts: [],
    hasMorePosts: true,
    expandedPost: undefined,
    sharedPostId: undefined,
    userId: undefined
};

const mapStateToProps = rootState => ({
    posts: rootState.posts.posts,
    hasMorePosts: rootState.posts.hasMorePosts,
    expandedPost: rootState.posts.expandedPost,
    userId: rootState.profile.user.id,
    editablePost: rootState.posts.editablePost,
    postReactedUsers: rootState.posts.postReactedUsers
});

const actions = {
    loadPosts,
    loadMorePosts,
    likePost,
    toggleExpandedPost,
    addPost,
    deletePost,
    getUpdatePost,
    updatePost,
    restorePost,
    getReactedUsers
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Thread);
