import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
// import * as userService from 'src/services/authService'
import {
    ADD_POST,
    LOAD_MORE_POSTS,
    SET_ALL_POSTS,
    SET_EXPANDED_POST,
    SET_EDITABLE_POST,
    SET_EDITABLE_COMMENT,
    SET_COMMENT_REACTED_USERS,
    SET_POST_REACTED_USERS
} from './actionTypes';

const setPostsAction = posts => ({
    type: SET_ALL_POSTS,
    posts
});

const setEditablePost = post => ({
    type: SET_EDITABLE_POST,
    post
})

const addMorePostsAction = posts => ({
    type: LOAD_MORE_POSTS,
    posts
});

const addPostAction = post => ({
    type: ADD_POST,
    post
});

const setExpandedPostAction = post => ({
    type: SET_EXPANDED_POST,
    post
});

const setEditableComment = comment => ({
    type: SET_EDITABLE_COMMENT,
    comment
})

const setPostReactedUsers = users => ({
    type: SET_POST_REACTED_USERS,
    users
})

const setCommentReactedUsers = users => ({
    type: SET_COMMENT_REACTED_USERS,
    users
})

export const loadPosts = filter => async (dispatch) => {
    const posts = await postService.getAllPosts(filter);
    console.log(posts);
    dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
    const { posts: { posts } } = getRootState();
    const loadedPosts = await postService.getAllPosts(filter);
    const filteredPosts = loadedPosts
        .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
    dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async (dispatch) => {
    const post = await postService.getPost(postId);
    dispatch(addPostAction(post));
};

export const addPost = post => async (dispatch) => {
    const { id } = await postService.addPost(post);
    const newPost = await postService.getPost(id);
    dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    console.log(post);
    dispatch(setExpandedPostAction(post));
};

export const likePost = (postId, isLike) => async (dispatch, getRootState) => {
    const { id } = await postService.likePost(postId, isLike);
    const { likeCount, dislikeCount } = await postService.getPost(postId);

    const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
    const mapLikes = post => ({
        ...post,
        likeCount: !isNaN(likeCount) ? likeCount : (isLike ? Number(post.likeCount) + diff : post.likeCount),
        dislikeCount: !isNaN(dislikeCount) ? dislikeCount : (!isLike ? Number(post.dislikeCount) + diff : post.dislikeCount),
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapLikes(expandedPost)));
    }
};

export const addComment = request => async (dispatch, getRootState) => {
    const { id } = await commentService.addComment(request);
    const comment = await commentService.getComment(id);

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) + 1,
        comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const { posts: { posts, expandedPost } } = getRootState();
    console.log(posts)
    const updated = posts.map(post => (post.id !== comment.postId
        ? post
        : mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const getEditableComment = commentId => async (dispatch, getRootState) => {

    const editableComment = await commentService.getComment(commentId);
    console.log('editableComment', editableComment)
    dispatch(setEditableComment(editableComment));

};


export const deletePost = postId => async (dispatch, getRootState) => {
    const resp = postService.deletePost(postId);
    if (resp) {
        const { posts: { posts } } = getRootState();
        const updated = posts.map(post => post.id !== postId ? post : Object.assign(post, { deleted: true }));
        dispatch(setPostsAction(updated));
    }

};


export const deleteComment = (commentId, commentPostId) => async (dispatch, getRootState) => {
    const response = await commentService.deleteComment(commentId);
    console.log('response delete comment', response);
    if (response.deleted) {
        //  const thisPost = commentPostId ? await postService.getPost(commentPostId) : undefined;
        const mapComments = post => ({
            ...post,
            commentCount: Number(post.commentCount) - 1,
            comments: post.comments ? post.comments.map(comment => comment.id !== commentId ?
                comment : Object.assign(comment, { deleted: true })) : [],
        });

        const { posts: { posts, expandedPost } } = getRootState();
        console.log(posts)
        const updated = posts.map(post => (post.id !== commentPostId
            ? post
            : mapComments(post)));

        dispatch(setPostsAction(updated));
        if (expandedPost && expandedPost.id === commentPostId) {
            dispatch(setExpandedPostAction(mapComments(expandedPost)));
        }
    }
}
export const getUpdatePost = postId => async (dispatch, getRootState) => {
    const post = await postService.getPost(postId);
    dispatch(setEditablePost(post))
}


export const updatePost = editablePost => async (dispatch, getRootState) => {

    const response = await postService.updatePost(editablePost);
    if (response.updated) {
        const updatedPost = await postService.getPost(editablePost.id);//not Sure if this needs
        const { posts: { posts } } = getRootState();
        const updated = posts.map(post => (post.id !== editablePost.id ? post : updatedPost));
        dispatch(setPostsAction(updated));
    }
};


export const restorePost = postId => async (dispatch, getRootState) => {

    const response = await postService.restorePost(postId);
    if (response.restored) {
        // const updatedPost = await postService.getPost(editablePost.id);
        const { posts: { posts } } = getRootState();
        const updated = posts.map(post => (post.id !== postId ? post : { ...post, deleted: false }));
        dispatch(setPostsAction(updated));
    }
};

export const restoreComment = (commentId, postId) => async (dispatch, getRootState) => {

    const response = await commentService.restoreComment(commentId);
    if (response.restored) {
        console.log('restoreComment')
        // const updatedPost = await postService.getPost(editablePost.id);
        const { posts: { posts, expandedPost } } = getRootState();
        const mapComments = post => ({
            ...post,
            comments: post.comments ? post.comments.map(comment => comment.id !== commentId ? comment : { ...comment, deleted: false })
                : []
            // comment is taken from the current closure
        });
        const updated = posts.map(post => (post.id !== postId
            ? post
            : mapComments(post)));

        dispatch(setPostsAction(updated));

        if (expandedPost && expandedPost.id === postId) {
            dispatch(setExpandedPostAction(mapComments(expandedPost)));
        }
    }
};

export const updateComment = editableComment => async (dispatch, getRootState) => {

    const response = await commentService.updateComment(editableComment);
    if (response.updated) {
        console.log('commentService.updateComment', editableComment.postId)
        // const updatedComment = await commentService.getComment(editableComment.id);
        const { posts: { posts, expandedPost } } = getRootState();
        const mapComments = post => ({
            ...post,
            comments: post.comments ? post.comments.map(comment => comment.id !== editableComment.id ? comment : editableComment)
                : [editableComment]
            // comment is taken from the current closure
        });
        const updated = posts.map(post => (post.id !== editableComment.postId
            ? post
            : mapComments(post)));

        dispatch(setPostsAction(updated));

        if (expandedPost && expandedPost.id === editableComment.postId) {
            dispatch(setExpandedPostAction(mapComments(expandedPost)));
        }
    }
};

export const getReactedUsers = (data) => async (dispatch, getRootState) => {
    const users = await postService.getReactedUsers(data);
    data.postId ? dispatch(setPostReactedUsers(users)) : dispatch(setCommentReactedUsers(users));
    console.log(users);

};

export const likeComment = (commentId, isLike) => async (dispatch, getRootState) => {
    const { id } = await commentService.likeComment(commentId, isLike);
    const updatedComment = await commentService.getComment(commentId);
    const { likeCount, dislikeCount } = updatedComment;
    const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
    const mapLikes = comment => ({
        ...comment,
        likeCount: !isNaN(likeCount) ? Number(likeCount) : (isLike ? Number(comment.likeCount) + diff : comment.likeCount),
        dislikeCount: !isNaN(dislikeCount) ? Number(dislikeCount) : (!isLike ? Number(comment.dislikeCount) + diff : comment.dislikeCount),
    });

    const { posts: { expandedPost } } = getRootState();

    const comments = expandedPost.comments.map(comment => comment.id !== commentId ? comment : mapLikes(comment));
    dispatch(setExpandedPostAction({ ...expandedPost, comments }));

};

