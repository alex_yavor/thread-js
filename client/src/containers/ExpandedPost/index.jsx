import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
    likePost, toggleExpandedPost, addComment, deleteComment,
    getEditableComment, updateComment, restoreComment, getReactedUsers, likeComment
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

class ExpandedPost extends React.Component {
    state = {
        open: true,
        showCommentEditor: false
    };

    closeModal = () => {
        this.props.toggleExpandedPost();
    }

    editComment = (commentId) => {
        this.props.getEditableComment(commentId);
        this.setState({ showCommentEditor: true })

    }
    closeCommentEditor = () => {
        this.setState({ showCommentEditor: false })
    }



    render() {
        const { post, sharePost, ...props } = this.props;
        const { showCommentEditor } = this.state;
        return (
            <Modal centered={false} open={this.state.open} onClose={this.closeModal}>
                {post
                    ? (
                        <Modal.Content>
                            <Post
                                post={post}
                                likePost={props.likePost}
                                toggleExpandedPost={props.toggleExpandedPost}
                                sharePost={sharePost}
                                getReactedUsers={props.getReactedUsers}
                                postReactedUsers={props.postReactedUsers}
                            />
                            <CommentUI.Group style={{ maxWidth: '100%' }}>
                                <Header as="h3" dividing>
                                    Comments
                                </Header>
                                {post.comments && post.comments
                                    .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                                    .map(comment => <Comment
                                        isOwner={this.props.userId === comment.user.id ? true : false}
                                        key={comment.id}
                                        comment={comment}
                                        deleteComment={props.deleteComment}
                                        editComment={this.editComment}
                                        restoreComment={props.restoreComment}
                                        getReactedUsers={props.getReactedUsers}
                                        commentReactedUsers={props.commentReactedUsers}
                                        likeComment={props.likeComment} />)
                                }
                                <AddComment postId={post.id} addComment={props.addComment} />
                            </CommentUI.Group>
                            {
                                showCommentEditor
                                && <Modal open onClose={this.closeCommentEditor}>
                                    <Modal.Content>
                                        <AddComment
                                            postId={post.id}
                                            addComment={props.updateComment}
                                            comment={props.editablePost}
                                            close={this.closeCommentEditor} />
                                    </Modal.Content> </Modal>
                            }
                        </Modal.Content>
                    )
                    : <Spinner />
                }
            </Modal>
        );
    }
}

ExpandedPost.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    likePost: PropTypes.func.isRequired,
    addComment: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired,
    deleteComment: PropTypes.func.isRequired,
    getEditableComment: PropTypes.func.isRequired,
    updateComment: PropTypes.func.isRequired,
    restoreComment: PropTypes.func.isRequired,
    getReactedUsers: PropTypes.func.isRequired,
    likeComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
    post: rootState.posts.expandedPost,
    userId: rootState.profile.user.id,
    editablePost: rootState.posts.editableComment,
    commentReactedUsers: rootState.posts.commentReactedUsers,
    postReactedUsers: rootState.posts.postReactedUsers

});
const actions = {
    likePost,
    toggleExpandedPost,
    addComment,
    deleteComment,
    getEditableComment,
    updateComment,
    restoreComment,
    getReactedUsers,
    likeComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ExpandedPost);
