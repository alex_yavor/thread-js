import callWebApi from 'src/helpers/webApiHelper';

export const getAllPosts = async (filter) => {
    const response = await callWebApi({
        endpoint: '/api/posts',
        type: 'GET',
        query: filter
    });
    return response.json();
};

export const addPost = async (request) => {
    const response = await callWebApi({
        endpoint: '/api/posts',
        type: 'POST',
        request
    });
    return response.json();
};


export const getPost = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/posts/${id}`,
        type: 'GET'
    });
    return response.json();
};


export const likePost = async (postId, isLike) => {
    const response = await callWebApi({
        endpoint: '/api/posts/react',
        type: 'PUT',
        request: {
            postId,
            isLike
        }
    });
    return response.json();
};


export const deletePost = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/posts/${id}`,
        type: 'DELETE'
    });
    return response.json();
};

export const updatePost = async (request) => {
    const response = await callWebApi({
        endpoint: '/api/posts',
        type: 'PUT',
        request
    });
    return response.json();
};

export const restorePost = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/posts/restore/${id}`,
        type: 'GET'
    });
    return response.json();
};

export const getReactedUsers = async (data) => {
    const query = (data.postId ? `postId=${data.postId}` : `commentId=${data.commentId}`) + `&isLike=${data.isLike}`
    console.log(query);
    const response = await callWebApi({
        endpoint: `/api/posts/reaction?${query}`,
        type: 'GET'
    });
    return response.json();
};


// should be replaced by approppriate function
export const getPostByHash = async hash => getPost(hash);
