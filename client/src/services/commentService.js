import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async (request) => {
    const response = await callWebApi({
        endpoint: '/api/comments',
        type: 'POST',
        request
    });
    return response.json();
};

export const getComment = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: 'GET'
    });
    return response.json();
};

export const deleteComment = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: 'DELETE'
    });
    return response.json();
};

export const updateComment = async (request) => {
    const response = await callWebApi({
        endpoint: '/api/comments',
        type: 'PUT',
        request
    });
    return response.json();
};

export const restoreComment = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/comments/restore/${id}`,
        type: 'GET'
    });
    return response.json();
};

export const getReactedUsers = async ({ commentId, isLike }) => {
    const response = await callWebApi({
        endpoint: `/api/comments/${commentId}?${isLike}`,
        type: 'GET'
    });
    return response.json();
};

export const likeComment = async (commentId, isLike) => {
    const response = await callWebApi({
        endpoint: '/api/comments/react',
        type: 'PUT',
        request: {
            commentId,
            isLike
        }
    });
    return response.json();
};