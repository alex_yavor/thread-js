import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';

class AddComment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            body: ''
        };
    }

    componentDidUpdate(prevProps) {
        if (this.props.comment !== prevProps.comment) {
            const comment = this.props.comment;
            this.setState({
                body: comment.body,

            })
        }
    }

    handleAddComment = async () => {
        const { body } = this.state;
        if (!body) {
            return;
        }
        const { postId } = this.props;
        const id = this.props.comment ? this.props.comment.id : undefined;
        await this.props.addComment({ ...this.props.comment, body, postId });
        this.setState({ body: '' });
        id && this.props.close();
    }

    render() {
        const { body } = this.state;
        return (
            <Form reply onSubmit={this.handleAddComment}>
                <Form.TextArea
                    value={body}
                    placeholder="Type a comment..."
                    onChange={ev => this.setState({ body: ev.target.value })}
                />
                <Button type="submit" content="Post comment" labelPosition="left" icon="edit" primary />
            </Form>
        );
    }
}

AddComment.propTypes = {
    addComment: PropTypes.func.isRequired,
    postId: PropTypes.string.isRequired
};

export default AddComment;
