import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';

import Reaction from '../Reaction';
const Post = ({ post,
    likePost,
    toggleExpandedPost,
    sharePost,
    canEdit,
    editPost,
    deletePost,
    restorePost,
    getReactedUsers,
    postReactedUsers }) => {

    const {
        id,
        image,
        body,
        user,
        likeCount,
        dislikeCount,
        commentCount,
        createdAt,
        deleted
    } = post;
    const date = moment(createdAt).fromNow();
    if (deleted) {
        return <Card style={{ width: '100%' }}>
            <Card.Content>
                <Card.Meta>Post was deleted. <a  href='#' onClick={() => restorePost(id)}>Restore post</a></Card.Meta>

            </Card.Content>
        </Card>
    }
    return (

        <Card style={{ width: '100%' }}>
            {image && <Image src={image.link} wrapped ui={false} />}
            <Card.Content>
                <Card.Meta>
                    <span className="date">
                        posted by
                        {' '}
                        {user.username}
                        {' - '}
                        {date}
                    </span>
                    {canEdit && <a href='#' onClick={() => editPost(id)}>edit</a>}
                    {canEdit && <a href='#' onClick={() => deletePost(id)}>delete</a>}
                </Card.Meta>
                <Card.Description>
                    {body}
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                <Reaction
                    likeCount={Number(likeCount)}
                    dislikeCount={Number(dislikeCount)}
                    likePost={likePost}
                    id={id}
                    getReactedUsers={getReactedUsers}
                    postReactedUsers={postReactedUsers}
                ></Reaction>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
                    <Icon name="comment" />
                    {commentCount}
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
                    <Icon name="share alternate" />
                </Label>
            </Card.Content>
        </Card>
    );
};


Post.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    likePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired,
    canEdit: PropTypes.bool,
    postReactedUsers: PropTypes.array
};

export default Post;
