import React from 'react';
import PropTypes from 'prop-types';
import { Image, Label, Icon, Popup } from 'semantic-ui-react';

import styles from './styles.module.scss';
class Reaction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: []
        };
    }
    componentDidUpdate(prevProps) {
        if (this.props.postReactedUsers !== prevProps.postReactedUsers) {
            const users = this.props.postReactedUsers;
            this.setState({
                users
            })
        }
    }

    getUsers = (data) => {
        this.props.getReactedUsers(data);
    }
    //пользователей меньше чем лайков так как в бд есть пользователи, лайкнувшие один пост несколько раз
    createPopup = (count, isLike) => {
        const { likePost, id, fromComment } = this.props;
        const { users } = this.state;
        const defaultImg = 'https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png';
        const namedId = fromComment ? 'commentId' : 'postId';
        return <Popup
            offset='0, 10px'
            position='bottom center'
            trigger={
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id, isLike)}>
                    <Icon name="thumbs up" />
                    {count}
                </Label>

            }
            onOpen={() => this.getUsers({ [namedId]: id, isLike })}
            onClose={() => this.setState({ users: [] })}>
            
            <Popup.Content>
               {users&& users.map(user => <Image key={user.id} src={user.image ? user.image.link : defaultImg} alt={user.username} avatar />)}
            </Popup.Content>
        </Popup>
    }
    render() {
        const { likeCount, dislikeCount } = this.props;
        return (
            <span>
                {this.createPopup(likeCount, true)}
                {this.createPopup(dislikeCount, false)}
            </span>
        )

    }
}

Reaction.propTypes = {
    postReactedUsers: PropTypes.array,
    likePost: PropTypes.func.isRequired,
    id: PropTypes.string,
    likeCount: PropTypes.any,
    dislikeCount: PropTypes.any
}

export default Reaction;