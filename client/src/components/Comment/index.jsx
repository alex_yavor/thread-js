import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';
import Reaction from '../../components/Reaction';

const Comment = (props) => {
    const {
        comment: {
            likeCount: likes,
            dislikeCount: dislikes,
            body,
            createdAt,
            user,
            id,
            postId,
            deleted,
            commentReactions = []
        },
        isOwner,
        deleteComment,
        editComment,
        restoreComment,
        likeComment,
        getReactedUsers,
        commentReactedUsers } = props;

    const date = moment(createdAt).fromNow();
    let likeCount = 0, dislikeCount = 0;
    commentReactions.forEach(commentReaction => {
        commentReaction.isLike ? likeCount++ : dislikeCount++;
    })

    likeCount = likes || likeCount;
    dislikeCount = dislikes || dislikeCount;

    if (deleted) {
        return <CommentUI className={styles.comment}>
            <CommentUI.Content>

                <CommentUI.Actions>
                    <CommentUI.Metadata>Comment was deleted. &nbsp;</CommentUI.Metadata>
                    <CommentUI.Action onClick={() => restoreComment(id, postId)}> Restore</CommentUI.Action>
                </CommentUI.Actions>


            </CommentUI.Content>
        </CommentUI>
    }
    return (
        <CommentUI className={styles.comment}>
            <CommentUI.Avatar src={getUserImgLink(user.image)} />
            <CommentUI.Content>
                <CommentUI.Author as="a">
                    {user.username}
                </CommentUI.Author>
                <CommentUI.Metadata>
                    {date}
                </CommentUI.Metadata>
                <CommentUI.Metadata>
                    {user.status}
                </CommentUI.Metadata>
                <CommentUI.Text>
                    {body}
                </CommentUI.Text>
                {isOwner ? <CommentUI.Actions>
                    <CommentUI.Action onClick={() => editComment(id)}>Edit</CommentUI.Action>
                    <CommentUI.Action onClick={() => deleteComment(id, postId)} >Delete</CommentUI.Action>
                </CommentUI.Actions> : null}
                <CommentUI.Content >
                    <Reaction
                        likeCount={likeCount}
                        dislikeCount={dislikeCount}
                        likePost={likeComment}
                        id={id}
                        getReactedUsers={getReactedUsers}
                        postReactedUsers={commentReactedUsers}
                        fromComment={true}
                    ></Reaction>
                </CommentUI.Content>
            </CommentUI.Content>
        </CommentUI>
    );
};

Comment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired,
    isOwner: PropTypes.bool,
    deleteComment: PropTypes.func.isRequired,
    editComment: PropTypes.func.isRequired,
    restoreComment: PropTypes.func.isRequired,
    likeComment: PropTypes.func.isRequired,
    getReactedUsers: PropTypes.func.isRequired,
    commentReactedUsers: PropTypes.array
};

export default Comment;
